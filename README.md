# Laravel Workerman
## 发布配置
php artisan vendor:publish --tag=workerman
## 开发
1. 实现`Myaccountl\LaravelWorkerman\Events\WorkermanEventInterface` 接口按不同事件进行业务逻辑编写<br>
2. 者继承 `Myaccountl\LaravelWorkerman\Events\EventsHandle` 
   该类已经实现了`WorkermanEventInterface` 并实现了基础的用户绑定、广播分组加入和OnConnect连接事件,该事件传入$client_id可绑定`Myaccountl\LaravelWorkerman\Events\WSConnectEvent`实现自己的websocket连接事件业务，
3. 重写该该类的OnMessage方法时须调用parent::OnMessage 否则用户绑定和广播分组加入将不可用
   
## 使用
1. ### Windows
    > `php artisan workerman --s=register`  // 启动注册中心<br>
      `php artisan workerman --s=gateway`   // 启动网关<br>
      `php artisan workerman --s=worker`    // 启动业务处理<br>

2. ### Linux
   > `php artisan workerman <action> {--d} {--s=*}` // 操作wokerman启动 停止 重启 查看状态等<br>
     ###### 参数说明： <br>
   > - action: <br>
   > -  -  start   -- 启动<br>
   > -  -  stop    -- 停止<br>
   > - -  restart -- 重启<br>
   > - -  status  -- 查看状态<br>
   > -  --d: 后台启动<br>
   > -  --s: 要启动的服务 默认：all 启动register gateway  worker 所有服务<br>
   > -  -    参数值为：all register gateway worker 中的一个<br>
   > -  -    示例：`php artisan workerman start --s=gateway --s=worker`<br>
                 
## 使用示例
#### 一、客户端发送
``` JavaScript
// 加入分组
{"type": "join", "group": "broadcast:user:channel"}
// 心跳 每30秒发送一次
{"type": "ping"}
// 绑定用户 uid为用户加密ID
{"type": "bind", "uid": "YWRleUhWK25PVTQ9"}
```

#### 二、服务端发送消息到客户端
```PHP
use GatewayWorker\Lib\Gateway;

Gateway::sendToUid(7, json_encode(['type' => 'test', 'message' => 'user_send 测试消息！'], JSON_UNESCAPED_UNICODE))

Gateway::sendToGroup('broadcast:user:channel', json_encode(['type' => 'test', 'message' => 'group 测试消息！'], JSON_UNESCAPED_UNICODE))
```
