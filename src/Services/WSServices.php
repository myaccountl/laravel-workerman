<?php

namespace Myaccountl\LaravelWorkerman\Services;

use Myaccountl\LaravelTools\WsJsonResponse;
use Myaccountl\LaravelWorkerman\Define\WebSocketDefine;
use Myaccountl\LaravelWorkerman\Define\WebSocketTypeDefine;
use Myaccountl\LaravelWorkerman\Gateway;
use Illuminate\Support\Facades\Log;

class WSServices
{

    /**
     * 绑定用户
     * @param $client_id
     * @param $data
     */
    public static function bindUid($client_id, $data) {
        try {
            // 解密
            $uid = des_decrypt(array_get($data, 'uid', ''), config('workerman.bind_uid_password'));
            if (!$uid) {
                // 如果UID处理失败则直接进行返回
                return;
            }
            $current_flag = false;
            if (Gateway::isUidOnline($uid)) {
                // 如果绑定的用户有在线的client 则解除用户绑定的所有client
                $client_ids = Gateway::getClientIdByUid($uid);

                if (is_array($client_ids)) {
                    foreach ($client_ids as $value) {
                        if ($client_id == $value) {
                            // 判断用户是否绑定当前客户端
                            $current_flag = true;
                            continue;
                        }
                        // 解绑用户和client
                        Gateway::unbindUid($value, $uid);
                        // 关闭客户端
                        Gateway::closeClient($value);
                    }
                }
            }
            if (!$current_flag) {
                // 如果不是当前客户端则进行客户端与用户绑定
                Gateway::bindUid($client_id, $uid);
            }
            Gateway::sendToUid($uid, WsJsonResponse::success(WebSocketTypeDefine::BIND, null, '绑定成功！'));
        }catch (\Exception $e) {
            Log::error($e->getMessage());
        }
    }

    /**
     * 加入分组
     * @param $client_id
     * @param $data
     */
    public static function joinGroup($client_id, $data) {
        try {
            $group = array_get($data, 'group');
            $group_channel_str = config('workerman.group_channel');
            $group_channel = [];
            if ($group_channel) {
                $group_channel = explode(',', $group_channel_str);
            } else {
                $group_channel = WebSocketDefine::GROUP_CHANNEL;
            }
            if (!in_array($group, $group_channel)) {
                return;
            }
            Gateway::joinGroup($client_id, $group);
            Gateway::sendToClient($client_id, WsJsonResponse::success(WebSocketTypeDefine::JOIN, null, '加入分组成功！'));
        } catch (\Exception $e) {
            Log::error($e->getMessage());
        }
    }
}
