<?php

namespace Myaccountl\LaravelWorkerman;

use Illuminate\Support\Facades\Log;

/**
 * @mixin \GatewayWorker\Lib\Gateway
 */
class Gateway
{
    public function __call($name, $arguments)
    {
        try {
            // TODO: Implement __call() method.
            return (new \GatewayWorker\Lib\Gateway)->$name(...$arguments);
        } catch (\Exception $e) {
            Log::error($e->getMessage());
        }
        return null;
    }

    public static function __callStatic($name, $arguments)
    {
        // TODO: Implement __callStatic() method.
        try {
            return \GatewayWorker\Lib\Gateway::$name(...$arguments);
        } catch (\Exception $e) {
            Log::error($e->getMessage());
        }
        return null;
    }
}
