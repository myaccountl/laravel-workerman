<?php

namespace Myaccountl\LaravelWorkerman\Controller;

use Myaccountl\LaravelWorkerman\Services\WSServices;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Myaccountl\LaravelTools\JsonResponse;
use Myaccountl\LaravelWorkerman\Define\WebSocketDefine;

class WorkermanController extends Controller
{
    use JsonResponse;
    /**
     * 绑定用户和websocket
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse|\Illuminate\Http\Resources\Json\JsonResource
     */
    public function bindUser(Request $request)
    {
        // 绑定客户端ID和用户
        WSServices::bindUid($request->client_id, des_encrypt($request->user()->id, config('workerman.bind_uid_password')));
        return $this->success(null, '绑定用户成功！');
    }

    /**
     * 加入消息广播组
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse|\Illuminate\Http\Resources\Json\JsonResource
     */
    public function joinBroadcastGroup(Request $request) {
        // 加入前端用户广播通道
        WSServices::joinGroup($request->client_id, ['group' => WebSocketDefine::BROADCAST_USER_CHANNEL]);
        return $this->success(null, '加入广播组成功！');
    }

    /**
     * 加入广播组和绑定用户
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse|\Illuminate\Http\Resources\Json\JsonResource
     */
    public function bindUserAndJoinBroadcastGroup(Request $request) {
        // 绑定客户端ID和用户
        WSServices::bindUid($request->client_id, des_encrypt($request->user()->id, config('workerman.bind_uid_password')));
        // 加入前端用户广播通道
        WSServices::joinGroup($request->client_id, ['group' => WebSocketDefine::BROADCAST_USER_CHANNEL]);
        return $this->success(null, '操作成功！');
    }

}
