<?php

namespace Myaccountl\LaravelWorkerman\Console;

use GatewayWorker\BusinessWorker;
use GatewayWorker\Gateway;
use GatewayWorker\Register;
use Illuminate\Console\Command;
use Workerman\Worker;

class WorkermanHandleConsole extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = '';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = '';

    protected $services = ['register', 'gateway', 'worker'];

    protected static $_OS = 'linux';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        self::checkSapiEnv();
        if (self::$_OS === 'linux') {
            $this->signature = 'workerman {action : 操作[start、stop、restart、status]} {--d : 后台启动} {--s=* : 要操作的服务[all,register,gateway,worker]}';
            $this->description = 'workerman服务操作 例: php artisan wokerman action --d --s=register --s=gateway';
        } else {
            $this->signature = 'workerman {--s= : 要操作的服务[gateway,register,worker]}';
            $this->description = 'workerman服务操作 例：php artisan wokerman --s=gateway';
        }
        parent::__construct();
    }

    protected static function checkSapiEnv()
    {
        // Only for cli.
        if (\PHP_SAPI !== 'cli') {
            exit("Only run in command line mode \n");
        }
        if (\DIRECTORY_SEPARATOR === '\\') {
            self::$_OS = 'windows';
        }
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        if (self::$_OS == 'windows') {
            $this->startWindows();
        } else {
            $this->startLinux();
        }
        return 0;
    }

    private function parseService($service){
        if (!in_array($service, $this->services)) {
            $this->error('--s 参数只能是：' . implode('、', $this->services) . '！');
            exit(0);
        }
        switch ($service) {
            case 'register':
                $this->startRegister();
                break;
            case 'gateway':
                $this->startGateway();
                break;
            case 'worker':
                $this->startBusinessWorker();
                break;
        }
    }

    private function startWindows() {
        $service = $this->option('s');
        $this->parseService($service);
        global $args;
        $args[0] = '-q';
        Worker::runAll();
    }

    private function startLinux() {
        $this->services[] = 'all';
        global $argv;
        $action = $this->argument('action');
        $d = $this->option('d') ? '-d' : '';
        $services = $this->option('s');
        if (!$services) {
            $services = ['all'];
        }
        if (in_array('all', $services)) {
            $argv[0] = 'all';
            $this->startRegister();
            $this->startGateway();
            $this->startBusinessWorker();
        } else {
            $argv[0] = implode('|', $services);
            foreach ($services as $service) {
                $this->parseService($service);
            }
        }
        $argv[1] = $action;
        $argv[2] = $d;
        Worker::runAll();
    }

    /**
     * 启动注册中心
     */
    private function startRegister() {
        $host = config('workerman.register.host', '0.0.0.0');
        $port = config('workerman.register.port', '1236');
        // 必须是workerman 定义的text协议
        new Register("text://{$host}:{$port}");
    }

    /**
     * 启动网关
     */
    private function startGateway() {
        $host = config('workerman.gateway.host', '0.0.0.0');
        $port = config('workerman.gateway.port', '1237');
        $name = config('workerman.gateway.name', 'LaravelWorkManGateway');
        $process_number = config('workerman.gateway.process_number', '1');
        $lan_ip = config('workerman.gateway.lan_ip', '127.0.0.1');
        $start_port = config('workerman.gateway.start_port', 4000);
        $register_address = config('workerman.register.link_address', '127.0.0.1:1236');
        $ping = config('workerman.gateway.ping', ['interval' => 50, 'limit' => 1, 'ping_data' => '', 'server_send' => false]);
        $server_send = array_get($ping, 'server_send', false);
        $data = '';
        if ($server_send) {
            $data = array_get($ping, 'data', '');
            if (!$data) {
                $this->error('开启服务端心跳数据发送时，心跳数据不能为空！');
            }
        }
        $gateway = new Gateway("websocket://{$host}:{$port}");
        // gateway名称，status方便查看
        $gateway->name = $name;
        // gateway进程数
        $gateway->count = $process_number;
        // 本机ip，分布式部署时使用内网ip 真实内网IP不是能是0.0.0.0
        $gateway->lanIp = $lan_ip;
        // 内部通讯起始端口，假如$gateway->count=4，起始端口为4000
        // 则一般会使用4000 4001 4002 4003 4个端口作为内部通讯端口
        $gateway->startPort = $start_port;
        // 服务注册地址
        $gateway->registerAddress = $register_address;
        // 心跳超时时间
        $gateway->pingInterval = array_get($ping, 'interval', 50);
        // 没有收到心跳数据的次数
        $gateway->pingNotResponseLimit = array_get($ping, 'limit', 1);
        // 心跳数据
        $gateway->pingData = $data;
        // 心跳说明  pingInterval * pingNotResponseLimit = 60
        // : 60秒内没有任何数据传输给服务端则服务端任务对应客户端已经掉线服务端关闭链接并出发onClose回调
    }

    /**
     * 启动工作进程
     */
    private function startBusinessWorker() {
        $name = config('workerman.worker.name', 'LaravelWorkermanBusinessWorker');
        $count = config('workerman.worker.process_number', 1);
        $registerAddress = config('workerman.register.link_address', '127.0.0.1:1236');
        $event_handler = config('workerman.worker.event_handler', 'Event');
        $worker = new BusinessWorker();
        $worker->name = $name;
        $worker->count = $count;
        $worker->registerAddress = $registerAddress;
        $worker->eventHandler = $event_handler;
    }
}
