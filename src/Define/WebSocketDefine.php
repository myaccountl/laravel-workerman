<?php

namespace Myaccountl\LaravelWorkerman\Define;

interface WebSocketDefine
{
    /**
     * 加密秘钥
     */
    const PASSWORD = 'bind:user:password';
    /**
     * 客户端用户广播通道
     */
    const BROADCAST_USER_CHANNEL = 'broadcast:user:channel';

    /**
     * 所有通道组
     */
    const GROUP_CHANNEL = [
        self::BROADCAST_USER_CHANNEL
    ];
}
