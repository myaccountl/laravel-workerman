<?php

namespace Myaccountl\LaravelWorkerman\Define;

interface WebSocketTypeDefine
{
    const INIT = 'init';
    const BIND = 'bind';
    const JOIN = 'join';
    const PING = 'ping';
}
