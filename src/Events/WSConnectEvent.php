<?php

namespace Myaccountl\LaravelWorkerman\Events;

use Illuminate\Broadcasting\Channel;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Broadcasting\PresenceChannel;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Queue\SerializesModels;

class WSConnectEvent
{
    use Dispatchable, InteractsWithSockets, SerializesModels;
    public $client_id;
    /**
     * Create a new event instance.
     *
     * @return void
     */
    public function __construct($client_id)
    {
        //
        $this->client_id = $client_id;
    }

}
