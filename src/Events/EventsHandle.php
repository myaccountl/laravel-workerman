<?php

namespace Myaccountl\LaravelWorkerman\Events;

use Myaccountl\LaravelWorkerman\Define\WebSocketTypeDefine;
use Myaccountl\LaravelTools\WsJsonResponse;
use Myaccountl\LaravelWorkerman\Services\WSServices;
use GatewayWorker\Lib\Gateway;

class EventsHandle implements WorkermanEventInterface
{

    public static function onConnect($client_id)
    {
        // TODO: Implement onConnect() method.
        Gateway::sendToClient($client_id, WsJsonResponse::success(WebSocketTypeDefine::INIT, ['client_id' => $client_id]));
        // 触发链接事件
        event(new WSConnectEvent($client_id));
    }

    public static function onMessage($client_id, $message)
    {
        // TODO: Implement onMessage() method.
        $data = json_decode($message, true);
        if ($data) {
            $type = array_get($data, 'type');
            if (!$type) {
                return ;
            }
            switch ($type) {
                case WebSocketTypeDefine::BIND:
                    WSServices::bindUid($client_id, $data);
                    break;
                case WebSocketTypeDefine::JOIN:
                    WSServices::joinGroup($client_id, $data);
                    break;
                case WebSocketTypeDefine::PING:
                    break;
            }
        }
    }

    public static function onClose($client_id)
    {
        // TODO: Implement onClose() method.
    }

    public static function onWorkerStart($worker)
    {
        // TODO: Implement onWorkerStart() method.
    }

    public static function onWebSocketConnect($client_id, $data)
    {
        // TODO: Implement onWebSocketConnect() method.
    }

    public static function onWorkerStop($worker)
    {
        // TODO: Implement onWorkerStop() method.
    }
}
