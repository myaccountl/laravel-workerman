<?php

namespace Myaccountl\LaravelWorkerman\Events;

use GatewayWorker\BusinessWorker;

/**
 * 主逻辑
 * 主要是处理 onConnect onMessage onClose 三个方法
 * onConnect 和 onClose 如果不需要可以不用实现并删除
 */
interface WorkermanEventInterface
{

    /**
     * 当businessWorker进程启动时触发。每个进程生命周期内都只会触发一次。
     * 可以在这里为每一个businessWorker进程做一些全局初始化工作，例如设置定时器，初始化redis等连接等。
     * 注意：$businessworker->onWorkerStart和Event::onWorkerStart不会互相覆盖，如果两个回调都设置则都会运行。
     * 不要在onWorkerStart内执行长时间阻塞或者耗时的操作，这样会导致BusinessWorker无法及时与Gateway建立连接，造成应用异常(SendBufferToWorker fail. The connections between Gateway and BusinessWorker are not ready错误)。
     * @param  BusinessWorker $worker businessWorker进程实例
     * @return mixed
     */
    public static function onWorkerStart($worker);

    /**
     * 当客户端连接上gateway完成websocket握手时触发的回调函数
     * 注意：此回调只有gateway为websocket协议并且gateway没有设置onWebSocketConnect时才有效。
     * @param string $client_id 固定为20个字符的字符串，用来全局标记一个socket连接，每个客户端连接都会被分配一个全局唯一的client_id。
     * @param array $data websocket握手时的http头数据，包含get、server等变量
     * @return mixed
     */
    public static function onWebSocketConnect($client_id, $data);

    /**
     * 当businessWorker进程退出时触发。每个进程生命周期内都只会触发一次。
     * 可以在这里为每一个businessWorker进程做一些清理工作，例如保存一些重要数据等。
     * 注意：某些情况将不会触发onWorkerStop，例如业务出现致命错误FatalError，或者进程被强行杀死等情况。
     * @param BusinessWorker $worker businessWorker进程实例
     * @return mixed
     */
    public static function onWorkerStop($worker);

    /**
     * 当客户端连接时触发
     * 如果业务不需此回调可以删除onConnect
     *
     * @param int $client_id 连接id
     */
    public static function onConnect($client_id);

   /**
    * 当客户端发来消息时触发
    * @param int $client_id 连接id
    * @param mixed $message 具体消息
    */
   public static function onMessage($client_id, $message);

   /**
    * 当用户断开连接时触发
    * @param int $client_id 连接id
    */
   public static function onClose($client_id);
}
