<?php
return [
    // 使用http绑定用户时所使用的guard 验证器
    'http_bind_auth_guard' => env('WORKERMAN_HTTP_BIND_AUTH_GUARD', 'sanctum'),
    'bind_uid_password' => env('WORKERMAN_BIND_UID_PASSWORD', \Myaccountl\LaravelWorkerman\Define\WebSocketDefine::PASSWORD),
    // 广播通道 只有配置的通道才能加入  可以为多个 英文逗号分隔 如:channel1,channel2,channel3,..... 默认值：broadcast:user:channel 前端用户广播通道
    'group_channel' => env('WORKERMAN_GROUP_CHANNEL', implode(',', \Myaccountl\LaravelWorkerman\Define\WebSocketDefine::GROUP_CHANNEL)),
    // 注册中心配置
    'register' => [
        'host' => env('WORKERMAN_REGISTER_HOST', '0.0.0.0'),
        'port' => env('WORKERMAN_REGISTER_PORT', 1236),
        // Gateway和BusinessWorker 连接注册中心的地址
        'link_address' => env('WORKERMAN_REGISTER_LINK_ADDRESS', '127.0.0.1:1236'),
    ],
    'gateway' => [
        'host' => env('WORKERMAN_GATEWAY_HOST', '0.0.0.0'),
        'port' => env('WORKERMAN_GATEWAY_PORT', 1237),
        'name' => env('APP_NAME') .'_' . env('WORKERMAN_GATEWAY_NAME', 'LaravelWorkManGateway'),
        // Gateway 进程数量
        'process_number' => env('WORKERMAN_GATEWAY_PROCESS_NUMBER', 1),
        'lan_ip' => env('WORKERMAN_GATEWAY_LAN_IP', '127.0.0.1'),
        // 给BusinessWorker提供链接服务的初始端口 如果process_number 大于1 则需要保证start_port 至 start_port + process_number 之间的所有端口可用
        'start_port' => env('WORKERMAN_GATEWAY_START_PORT', 4000),
        'ping' => [
            'interval' => env('WORKERMAN_GATEWAY_REGISTER_PING_INTERVAL', 50),
            // 为0时代表不发送心跳 服务端不会断开链接
            'limit' => env('WORKERMAN_GATEWAY_REGISTER_PING_LIMIT', 1),
            // 服务端发送心跳
            'server_send' => env('WORKERMAN_GATEWAY_REGISTER_PING_SERVER_SEND', false),
            // 服务端定时向客户端发送的心跳数据 服务端发送心跳时不能为空
            'data' => env('WORKERMAN_GATEWAY_REGISTER_PING_DATA', '')
        ]
    ],
    'worker' => [
        'name' => env('APP_NAME') .'_' . env('WORKERMAN_GATEWAY_WORKER_NAME', 'LaravelWorkermanBusinessWorker'),
        // BusinessWorker 进程数量
        'process_number' => env('WORKERMAN_GATEWAY_WORKER_PROCESS_NUMBER', 1),
        // 事件业务类 用户自己实现的业务逻辑 继承Myaccountl\LaravelWorkerman\Events\WorkermanEventInterface 并实现其中的方法
        'event_handler' => env('WORKERMAN_GATEWAY_WORKER_EVENT_HANDLER', 'Myaccountl\LaravelWorkerman\Events\EventsHandle')
    ]
];
